const keys = {
    a: false,
    d: false,
    s: false,
    w: false,
}

function hero(obj) {
    //get hero and explode image from all data array
    const heroImg = allImgData.find(item => item.name === "hero").img
    const explodeImg = allImgData.find(item => item.name === "explode").img
    ctx.beginPath()
    if(obj.explode) {
        //draw explode img and game is over
        ctx.drawImage(explodeImg, obj.x, obj.y, obj.w, obj.h)
        obj.gameOver = true
    } else{
        //draw hero img
        ctx.drawImage(heroImg, obj.x, obj.y, obj.w, obj.h)
    }
}

//key down events
function keyDownHandler(key, obj) {
    switch (key) {
        //left
        case "a":
            obj.a = true
            break;
        //right
        case "d":
            obj.d = true
            break;
        //up
        case "w":
            obj.w = true
            break;
        //down
        case "s":
            obj.s = true
            break;
    
        default:
            break;
    }
}

//key up events
function keyUpHandler(key, obj) {
    switch (key) {
        case "a":
            obj.a = false
            break;
        case "d":
            obj.d = false
            break;
        case "w":
            obj.w = false
            break;
        case "s":
            obj.s = false
            break;
    
        default:
            break;
    }
}

function update_hero(obj) {
    hero(obj)

    if (obj.gameOver) {
        obj.changeX = 0
        obj.changeY = 0
    }

    //updata x and y position
    obj.x += obj.changeX
    obj.y += obj.changeY

    //move x-axis
    if (keys.a) {
        obj.changeX = -5
    } else if (keys.d) {
        obj.changeX = 5
    } else {
        obj.changeX = 0
    }

    //move y-axis
    if (keys.w) {
        obj.changeY = -5
    } else if (keys.s) {
        obj.changeY = 5
    } else {
        obj.changeY = 0
    }

    //set boundaries constrain hero within canvas
    if (obj.x < 0) {
        obj.x = 0
    }

    if (obj.y < 0) {
        obj.y = 0
    }
    
    if (obj.x > w - obj.w) {
        obj.x = w - obj.w
    }

    if (obj.y > h - obj.h) {
        obj.y = h - obj.h
    }
}


addEventListener("keydown", ({key}) => {keyDownHandler(key, keys)})
addEventListener("keyup", ({key}) => {keyUpHandler(key, keys)})

