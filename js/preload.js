const canvas = document.querySelector('#myCanvas')
const ctx = canvas.getContext('2d')
const w = 500
const h = 700
//set image scale
const scale = 0.2

let srcs = [
    "background.png", 
    "explode.png", 
    "bullet1.png", 
    "bullet2.png", 
    "enemy1.png", 
    "enemy2.png", 
    "hero.png"
]

//all static data list
let allImgData = []

//all static data
let assets_data = {
    heroData: {
        x: w/2,
        changeX: 0,
        y: h - 50,
        changeY: 0,
        w: 0,
        h: 0,
        status: 1,
        explode: false,
        gameOver: false,
    },
    bullet_data_1: {
        w: 0,
        h: 0,
    },
    enemy_data_1: {
        name: "enemy1",
        w: 0,
        h: 0,
    },
    enemy_data_2: {
        name: "enemy2",
        w: 0,
        h: 0,
    }
}

setUp()

//preload image sources
async function loading(c) {
    let imgList = []
    for (let i = 0; i < srcs.length; i++) {
        const fetchImg = new Promise((reslove, reject) => {
            let img = new Image()
            img.onload = () => {
                //get image file name and Image object in an object
                reslove({
                    name: srcs[i].split(".")[0],
                    img
                })
            }
            img.src = "./img/" + srcs[i]
        })
        //push each promise to the array
        imgList.push(fetchImg)
    }

    const allLoaded = await Promise.all(imgList)

    if (allLoaded) {
        console.log("finish loading sources");
        allImgData = allLoaded

        for(let asset of allImgData) {
            if(asset.name === "hero"){
                let heroImg = asset.img
                let heroW = heroImg.width * scale;
                let heroH = heroImg.height * scale;
                //after loading modify hero data to the correct position
                assets_data.heroData.x = w/2 - heroW/2;
                assets_data.heroData.y = h - heroH/2 - 50;
                assets_data.heroData.w = heroW;
                assets_data.heroData.h = heroH;
            }

            if (asset.name === "bullet1") {
                //resize bullet image
                let bulletScale = 0.5
                let bulletImg1 = asset.img
                let bulletW = bulletImg1.width * bulletScale;
                let bulletH = bulletImg1.height * bulletScale;
                //after loading modify bullet image data width and height
                assets_data.bullet_data_1.w = bulletW;
                assets_data.bullet_data_1.h = bulletH;
            }

            if (asset.name === "enemy1") {
                //specify enemy1 scale to a smaller value
                let enemy_1_scale = 0.15
                let enemyImg = asset.img
                let enemyW = enemyImg.width * enemy_1_scale;
                let enemyH = enemyImg.height * enemy_1_scale;
                //after loading modify enemy1 image data width and height
                assets_data.enemy_data_1.w = enemyW;
                assets_data.enemy_data_1.h = enemyH;
            }

            if (asset.name === "enemy2") {
                let enemyImg = asset.img
                let enemyW = enemyImg.width * scale;
                let enemyH = enemyImg.height * scale;
                //after loading modify enemy2 image data width and height
                assets_data.enemy_data_2.w = enemyW;
                assets_data.enemy_data_2.h = enemyH;
            }
        }

        c()
    }
}

//create background scroll effect
let roll = 0

function bgScroll(bgIndex) {
    const bgImg = allImgData.find(item => item.name === "background").img
    roll += bgIndex
    //if bgImg height more than canvas set it back
    if(roll > h) {
        roll = 0
    }
    //create two image by turns 
    ctx.beginPath()
    ctx.drawImage(bgImg, 0, roll)
    ctx.beginPath()
    ctx.drawImage(bgImg, 0, roll - h)
}

function clear(){
    ctx.clearRect(0, 0, w, h)
}

function random(min, max) {
    return r = Math.round(Math.random() * (max - min) + min)
}

function setUp() {
    canvas.width = w
    canvas.height = h
    canvas.style.border = "5px solid orange"
}


addEventListener("load", function(){
    loading(gameStart)
})

