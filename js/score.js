let scoreCount = counter()

function counter() {
    let count = 0
    
    function increment(value) {
        count += value
    }

    function getCount() {
        return count
    }

    return {
        increment,
        getCount
    }
}

function score() {
    ctx.beginPath()
    ctx.font = "20px Arial"
    ctx.textAlign = "left"
    ctx.textBaseline = "middle"
    ctx.fillStyle = "white"
    ctx.fillText("score: " + scoreCount.getCount(), 30, 35)
    ctx.fill()
}
