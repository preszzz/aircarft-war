//game levels
let level = 230
let allEnemies = []


function enemies(obj) {
    //get enemies and explode image from all data array
    const enemyImg1 = allImgData.find(item => item.name === "enemy1").img
    const enemyImg2 = allImgData.find(item => item.name === "enemy2").img
    const explodeImg = allImgData.find(item => item.name === "explode").img
    ctx.beginPath()
    if(obj.explode) {
        //draw explode img and game is over
        obj.velocity = 0
        ctx.drawImage(explodeImg, obj.x, obj.y, obj.w, obj.h)
        setTimeout(() => {
            obj.isDeleted = true
        }, 500)
        
    } else{
        //draw enemies img
        if (obj.name === "enemy1") {
            ctx.drawImage(enemyImg1, obj.x, obj.y, obj.w, obj.h)
        } else if(obj.name === "enemy2"){
            ctx.drawImage(enemyImg2, obj.x, obj.y, obj.w, obj.h)
        }
    }
}

function update_enemy(obj) {
    obj.y += obj.velocity
    //when enemies is detroyed
    if (obj.hp <= 0) {
        obj.explode = true
        //only add score once
        if (!obj.scoreState) {
            scoreCount.increment(obj.score)
            obj.scoreState = true
        }
    }

    //when enemies move out of the canvas
    if (obj.velocity > 0 && obj.y > h + obj.h) {
        obj.isDeleted = true
    }

    //if the enemy move out of the canvas remove from array
    if (obj.isDeleted) {
        const index = allEnemies.indexOf(obj)
        allEnemies.splice(index, 1)
    }
}

function createEnemies(arr) {
    //set game levels
    let n = random(0, level)
    if (n <= 3) {
        //when n = 3 create enemy2
        if (n > 2) {
            arr.push({
                name: "enemy2",
                x: random(0, w - assets_data.enemy_data_2.w),
                y: -assets_data.enemy_data_2.h,
                velocity: random(1, 2),
                w: assets_data.enemy_data_2.w,
                h: assets_data.enemy_data_2.h,
                hp: 600,
                score: 10,
                scoreState: false,
                explode: false,
                isDeleted: false,
            })
        } else {
            //when n < 3 create enemy1
            arr.push({
                name: "enemy1",
                x: random(0, w - assets_data.enemy_data_1.w),
                y: -assets_data.enemy_data_1.h,
                velocity: random(2, 3),
                w: assets_data.enemy_data_1.w,
                h: assets_data.enemy_data_1.h,
                hp: 200,
                score: 5,
                scoreState: false,
                explode: false,
                isDeleted: false,
            })
        }
    } 
}