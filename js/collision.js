function collision(o1, o2) {
    if (
        o1.x + o1.w > o2.x &&       //left side
        o1.x < o2.x + o2.w &&       //right side
        o1.y + o1.h > o2.y &&       //top side
        o1.y < o2.y + o2.h          //bottom side
    ) {
        //collision detected
        return true
    } 
}

function collisionTestArray(obj, arr) {
    for (let i = 0; i < arr.length; i++) {
        const verifyCollision = collision(obj, arr[i])
        //bullet collide with enemy
        if (arr[i].hp > 0 && verifyCollision) {
            arr[i].hp -= obj.attack_value
            obj.isDeleted = true
            break;
        }
    }
}

function collisionAircraft(o1, o2) {
    const verifyCollision = collision(o1, o2)
    if (!o2.explode && o1.hp > 0 && verifyCollision) {
        o2.explode = true
    }
}
