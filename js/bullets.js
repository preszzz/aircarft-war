let gap = 10         //bullet gap
let gapNum = 0       //bullet timer
let allBullets = []  //bullet array
let isTrible = false


function bullet(obj) {
    const bulletImg1 = allImgData.find(item => item.name === "bullet1").img
    const bulletImg2 = allImgData.find(item => item.name === "bullet2").img
    ctx.beginPath()
    ctx.drawImage(bulletImg1, obj.x, obj.y, obj.w, obj.h)
}

function update_bullets(obj) {
    //move bullet
    obj.y += obj.velocity
    //when bullets move out of the canvas
    if (obj.velocity < 0 && obj.y < -obj.h) {
        obj.isDeleted = true
    }

    //if the bullet move out of the canvas remove from array
    if (obj.isDeleted) {
        const index = allBullets.indexOf(obj)
        allBullets.splice(index, 1)
    }
}

function createBullet(obj, arr) {
    gapNum++
    if (gapNum % gap === 0) {
        gapNum = 0
        if (isTrible) {
            //do it later
        } else {
            //push single bullet to the array
            arr.push({
                x: obj.x + obj.w/2 - assets_data.bullet_data_1.w/2,
                y: obj.y - obj.h/2 + assets_data.bullet_data_1.h,
                w: assets_data.bullet_data_1.w,
                h: assets_data.bullet_data_1.h,
                velocity: -7,
                attack_value: 100,
                isDeleted: false,
            })
        }
    }
}