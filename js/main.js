//game start
function gameStart() {
    document.querySelector("#loading").style.display = "none"
    animationLoop()
}

function gameOver() {
    ctx.beginPath()
    ctx.font = "40px Arial"
    ctx.fillStyle = "white"
    ctx.strokeStyle = "white"
    ctx.textAlign = "center"
    ctx.textBaseline = "middle"
    ctx.fillText("Game Over", w/2, h/2)
    ctx.strokeText("Game Over", w/2, h/2)
    ctx.fill()
}


function animationLoop() {
    clear()
    
    if (!assets_data.heroData.gameOver) {
        //background
        bgScroll(2)
        //hero character
        update_hero(assets_data.heroData)
        //bullet
        createBullet(assets_data.heroData, allBullets)
        for (let i = 0; i < allBullets.length; i++) {
            collisionTestArray(allBullets[i], allEnemies)
            bullet(allBullets[i])
            update_bullets(allBullets[i])
        }
        //enemies
        createEnemies(allEnemies)
        for (let i = 0; i < allEnemies.length; i++) {
            collisionAircraft(allEnemies[i], assets_data.heroData)
            enemies(allEnemies[i])
            update_enemy(allEnemies[i])
        }
        
    } else {
        bgScroll(0)
        hero(assets_data.heroData)
        for (let i = 0; i < allEnemies.length; i++) {
            enemies(allEnemies[i])
        }
        gameOver()
    }
    
    score()
    requestAnimationFrame(animationLoop)
}

